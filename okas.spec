Summary:	glen's (and others) vision of nice linux setup
Name:		okas
Version:	%{version}
Release:	%{release}
License:	BSD
Group:		Applications/System
URL:		https://glen.alkohol.ee/okas/
Conflicts:	keychain < 2.5.0
Conflicts:	poldek < 0.21-0.20070703.00.15.15
Conflicts:	poldek-delfi < 1.3
BuildArch:	noarch
BuildRoot:	%{tmpdir}/%{name}-%{version}-root-%(id -u -n)

%define		_datadir	%{_prefix}/share/%{name}
%define		_libdir		%{_prefix}/lib/%{name}

%define		tmpdir %(echo "${TMPDIR:-/tmp}")
%define		_unpackaged_files_terminate_build    0

# We can't let RPM do the dependencies automatic because it'll then pick up
# a correct but undesirable perl dependency from the utility scripts, which
# aren't really needed on all systems
AutoReqProv:	no

# force gz payload for broken rh7.3 hybrid
%define		_binary_payload	w9.gzdio

%description
This package adds some nice scripts, configures your bashrc and vim.
Also adds some XTerm .Xdefaults

This package is signed, you can import the key via:
$ rpm --import %{_datadir}/okas.asc

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


%clean
rm -rf $RPM_BUILD_ROOT

%prep

%build
%{__make}

%install
rm -rf $RPM_BUILD_ROOT
%{__make} install \
	DESTDIR=$RPM_BUILD_ROOT

%post
%{_libdir}/postin.sh

%preun
if [ "$1" = 0 ]; then
	%{_libdir}/preun.sh
fi

%triggerin -- vim-common, vim-rt
%{_libdir}/inst.sh vim

%triggerin -- XFree86
%{_libdir}/inst.sh XFree86

%triggerpostun -- %{name} < 1.5-1.506
%{_libdir}/inst.sh yum
%{_libdir}/inst.sh poldek

%files
%defattr(644,root,root,755)
%config(noreplace) %verify(not md5 mtime size) /etc/bashrc.local

%attr(755,root,root) %{_bindir}/*
%attr(755,root,root) %{_sbindir}/*

%dir %{_datadir}
%{_datadir}/XTerm.ad
%config %verify(not md5 mtime size) %{_datadir}/bashrc
%config %verify(not md5 mtime size) %{_datadir}/ssh-auth-sock
%{_datadir}/*.awk
%{_datadir}/*.bash
%{_datadir}/*.repo
%{_datadir}/DIR_COLORS
%{_datadir}/glen.kmap
%{_datadir}/glen.map
%{_datadir}/okas.asc
%{_datadir}/screenrc
%{_datadir}/subversion
%{_datadir}/sysctl.conf

%dir %{_datadir}/vim
%config %verify(not md5 mtime size) %{_datadir}/vim/okas.vim
%config %verify(not md5 mtime size) %{_datadir}/vim/vimrc

%dir %{_datadir}/contrib
%{_datadir}/contrib/bash_zsh_support
%{_datadir}/contrib/bash-preexec

%dir %{_libdir}
%{_libdir}/functions
%attr(755,root,root) %{_libdir}/inst.sh
%attr(755,root,root) %{_libdir}/postin.sh
%attr(755,root,root) %{_libdir}/preun.sh
%attr(755,root,root) %{_libdir}/uninst.sh

%{_mandir}/man1/*.1*
%{_mandir}/man5/okas.5*
