variables:
  PACKAGE_NAME: okas
  PACKAGECLOUD_PATH: glen/okas
  PACKAGECLOUD_TEST_PATH: glen/okas-test
  DEB_VERSION: debian/etch
  RPM_VERSION: fedora/14
  CONTAINER_BUILD_IMAGE: $CI_REGISTRY_IMAGE/builds:$CI_PIPELINE_ID-$CI_COMMIT_REF_SLUG

stages:
  - build
  - package
  - publish

.dind: &dind
  image: docker:latest
  services:
    - docker:dind

  before_script:
    - |
      push_image() {
        local source="$1" target="$2"
        docker pull "$source"
        docker tag "$source" "$target"
        docker push "$target"
      }
      env | grep -E '^CONTAINER_' | sort
      docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY

build:
  image: registry.gitlab.com/glensc/okas/build:build-v1
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: normal
  script:
    # use sane permissions until solved upstream
    # https://gitlab.com/gitlab-org/gitlab-runner/issues/1736
    - chmod -R a+rX,og-w .
    - libexec/update_timestamps.sh
    - make install DESTDIR=build prefix=/usr
  artifacts:
    paths:
      - ./build
    expire_in: 1 month

docker:
  <<: *dind
  stage: package
  dependencies:
    - "build"
  script:
    - apk add tar
    - tar -C build --owner=root --group=root -c . | docker import - $CONTAINER_BUILD_IMAGE
    - docker push $CONTAINER_BUILD_IMAGE

.rpmbuild: &rpmbuild
  stage: package
  image: registry.gitlab.com/glensc/okas/build:rpmbuild-v1
  dependencies:
    - "build"
  script:
    - libexec/build-okas.sh

.artifacts: &artifacts
  name: $PACKAGE_NAME
  paths:
    - ./dist/*.apk
    - ./dist/*.deb
    - ./dist/*.rpm

package:tsl:
  <<: *rpmbuild
  artifacts:
    <<: *artifacts
    expire_in: 1 month
  except:
    - tags

package:dsl:
  <<: *rpmbuild
  artifacts:
    <<: *artifacts
  only:
    - tags

.docker_publish: &docker_publish
  <<: *dind
  stage: publish
  script: |
    push_image $CONTAINER_BUILD_IMAGE $CONTAINER_RELEASE_IMAGE

docker branch:
  <<: *docker_publish
  variables:
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/branches:$CI_COMMIT_REF_SLUG
  except:
    - master
    - tags

docker master:
  <<: *docker_publish
  variables:
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest
  only:
    - master

docker tag:
  <<: *docker_publish
  variables:
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  only:
    - tags

.publish: &publish
  stage: publish
  image: registry.gitlab.com/glensc/okas/build:publish-v1
  script:
    - package_cloud push $PACKAGECLOUD_PATH/$RPM_VERSION publish/*.rpm
    - package_cloud push $PACKAGECLOUD_PATH/$DEB_VERSION publish/*.deb

.publish-pcloud: &publish-pcloud
  <<: *publish
  variables:
    GIT_STRATEGY: none
  before_script:
    - rm -rf publish
    - install -d publish
    - mv dist/*.rpm dist/*.deb publish

publish:tsl:
  <<: *publish-pcloud
  dependencies:
    - "package:tsl"
  variables:
    PACKAGECLOUD_PATH: $PACKAGECLOUD_TEST_PATH
    PACKAGECLOUD_TOKEN: $PACKAGECLOUD_TEST_TOKEN
    # duplicate from &publish-pcloud due yaml anchor merging works
    GIT_STRATEGY: none
  environment:
    name: packagecloud/test
    url: https://packagecloud.io/glen/okas-test
  except:
    - tags

publish:dsl:
  <<: *publish-pcloud
  dependencies:
    - "package:dsl"
  only:
    - tags
  environment:
    name: packagecloud/release
    url: https://packagecloud.io/glen/okas

publish:okas-dist:
  <<: *publish
  dependencies:
    - "package:tsl"
    - "package:dsl"
  tags:
    # use specific runner
    - okas-dist
  only:
    - tags
  environment:
    name: okas/release
    url: https://glen.alkohol.ee/okas/
  script:
    - libexec/build-dist.sh

# vim:ts=2:sw=2:et
