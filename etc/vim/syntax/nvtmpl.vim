" Vim syntax file
" Language:	templ
" Maintainer:	glen@online.ee
" Last Change:	1999 Sept 09
" Last Change:	12/04/2000 added html syntax
" Last Change:	02/02/2000 removed junk
" Last Change:  19/01/2001 vim 5.x/6.x compatability

" Remove any old syntax stuff hanging around
syn clear

if !exists("main_syntax")
  let main_syntax = 'templ'
endif

" Include more syntaxes
let php_sql_query = 1"
syn include @templHtml $VIMRUNTIME/syntax/php.vim
syn include @templSql $VIMRUNTIME/syntax/sql.vim

syn case match
syn region templHtmlcode matchgroup=templHtmlTag start="<HTMLCODE>"  keepend end="</HTMLCODE>" contains=@templHtml

" String
syn region templSqlStringDouble keepend matchgroup=None start=+"+ skip=+\\\\\|\\"+  end=+"+ contains=@templSql contained

syn keyword	templStatement	goto break return continue format call include
syn keyword	templStatement	useStyle execJava debug

syn keyword	templType	Provider new SqlCursor CSListCursor HttpGetPost
" constants have to be at least two stringTokes of lenght
syn match	templConst	"'\I\i*'" 
syn region	templProvider	start="\[" end="\]" contains=templString,templParen

syn keyword	templConditional	if else
syn keyword	templRepeat		while for do

syn keyword	templTodo		contained TODO FIXME XXX

" String and Character constants
" Highlight special characters (those which have a backslash) differently
syn match	templSpecial	contained "\\x\x\+\|\\\o\{1,3\}\|\\.\|\\$"
syn region	templString		start=+"+ skip=+\\\\\|\\"+ end=+"+ contains=templSpecial
syn match	templCharacter	"'[^\\]'"

"catch errors caused by wrong parenthesis
syn cluster	templParenGroup	contains=templParenError
syn region	templParen		transparent start='(' end=')' contains=ALLBUT,@templParenGroup
syn match	templParenError	")"
syn match	templInParen	contained "[{}]"

" comments
syn region	templComment	start="/\*" end="\*/" contains=templTodo
syn match	templComment	"//.*" contains=templTodo
syn match	templComment	"#.*" contains=templTodo
syn match	templCommentError	"\*/"


if !exists("did_templ_syntax_inits")
  let did_templ_syntax_inits = 1
  " The default methods for highlighting.  Can be overridden later
  hi link templConditional	Conditional
  hi link templRepeat	Repeat
  hi link templCharacter	Character
  hi link templParenError	templError
  hi link templInParen	templError
  hi link templCommentError	templError
  hi link templConst	Structure
  hi link templProvider	SpecialChar
  hi link templError	Error
  hi link templStatement	Statement
  hi link templType		Type
  hi link templCommentError	templError
  hi link templString	String
  hi link templComment	Comment
  hi link templSpecial	SpecialChar
  hi link templTodo		Todo
  "hi link cIdentifier	Identifier
  hi link templHtmlTag  htmlTag
endif

let b:current_syntax = "templ"

if main_syntax == 'templ'
  unlet main_syntax
endif

" vim: ts=4
