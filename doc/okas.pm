=head1 NAME

okas - okas

when unix invented two character commands, then okas makes them one :)

=head1 bash aliases

B<ls>, B<df>, B<du> all use human readable forms. (B<-h> switch)

B<cp>, B<mv> will ask you when you overwrite a file.

=head1 dns tools

=over 2

=item B<mx> I<HOST>

Lookup mx record for host.

=item B<soa> I<DOMAIN>

Lookup SOA for I<DOMAIN>.

=item B<nsa> I<HOST>

Do ANY query for I<HOST>.

=back



=head1 web tools

=over 2

=item B<hdrs> I<URL>

Fetch http headers from URL (does GET request) to be fully compatible (some
servers might have different behaviour if you do HEAD request).

=item B<ws>

Fetch L<http://localhost/server-status> and display it in terminal.

=item B<wr>

Show running Apache/Lightttpd requests from C<ws> output.

=item B<mailescape>

To avoid get emails caught by crawlers and spamspiders, there is way to "crypt"
email addresses. input stream is searched for email addresses and matched
addresses are replaced with html numeric entities every RFC-compiliant browser
can render these, and there's no visible change for user.

Example:

 $ echo "here's bill address: bill@microsoft.com" | mailescape
 replace [bill@microsoft.com] -> [&#98;&#105;&#108;&#108;&#64;&#109;&#105;&#99;&#114;&#111;&#115;&#111;&#102;&#116;&#46;&#99;&#111;&#109;]
 here's bill address: &#98;&#105;&#108;&#108;&#64;&#109;&#105;&#99;&#114;&#111;&#115;&#111;&#102;&#116;&#46;&#99;&#111;&#109;

 $ mailescape < index.html > new.html


=back



=head1 shell tools

=over 2

=item B<cronlogger> PROGRAM

Runs program, redirects all output to logfile. Output is also displayed on
screen if there's attached terminal. Intented to be run from cron.

The log is stored in C<$LOGDIR> with a indent detected from started program and
current date in filename, existing files are detected and incrementing serial
is appended to log filename.


ENVIRONMENT

 LOGDIR - defines directory where to store the logs. defaults to 'logs/'
 INDENT - enforce indent for logfile. defaults to one detected from PROGRAM.

=item B<pcat>

cat(1) null terminated strings.

 $ pcat /proc/$$/cmdline

=item B<punserialize>

dump files serialized by php.

=item B<zl>

Display contents of file, without empty lines and comments. the result is piped
do less(1), but it can be captured to file or another pipe.

 $ zl /etc/sysctl.conf | sort > a1
 $ zl /etc/sysctl.conf.rpmnew | sort > a2
 $ dif a[12]

=item B<diffcol>

This command will colourize unidiff according to vim colour schema. pipe output
of cvs diff, regular patch to it to see diff coloured nicely.

 $ cvs diff -u -r1.1 -r1.2 foo.c | diffcol

=item B<dif>

dif combines diffcol and less to one command.

it also excludes VCS control dirs C<.svn>, C<.bzr>, C<.git>, C<CVS>

 $ dif /etc/passwd~ /etc/passwd

=item B<d>

C<d> is alias showing you dif(1) of your last edited file:
 # vim /etc/passwd
 # d /etc/passwd
 --- /etc/passwd~    2009-11-20 12:13:38.000000000 +0200
 +++ /etc/passwd    2009-11-20 12:13:40.000000000 +0200
 @@ -1,4 +1,4 @@
 -root:x:0:0:root:/root:/bin/sh
 +root:x:0:0:root:/root:/bin/bash
  bin:x:1:1:bin:/bin:/sbin/nologin
  daemon:x:2:2:daemon:/sbin:/sbin/nologin
  adm:x:3:4:adm:/var/adm:/sbin/nologin

=item B<rgrep>

grep-r is alias to C<grep -r> with exclusion of VCS control dirs C<.svn>,
C<.bzr>, C<.git>, C<CVS>

 $ rgrep "sometext" .

=item B<clean>

Clean up all backups (I<*~>) recursively.

=item B<cleand>

Clean up all backups from current dir.

=item B<undos>

Convert filelist from DOS line endings to UNIX (CRLF -> CR).

=item B<unansi>

Remove ansi colour escapes from stream (STDIN).

=item B<cdwd>

chdir to current real directory (bash emulates symlinks).

it's also sometimes needed to cd back to current path (when someone has
replaced your current dir (rpm upgrade for example))

=item B<src>

Reloads your I<~/.bashrc>

=item B<nt>

Report status of TCP sockets. It's reporting summary of C<ss -nat> command. If
your host has a lot of sockets, load C<tcp_diag> module for kernel side
acceleration.

=item B<pcharmap>

Display all 256 chars in ASCII table with their hex and decimal values.

=item B<unurilfy>

Returns host part only from url. Useful to copy url for host based tools like, ping(1), traceroute(1).

=item B<upa>

upa is alias for C<(poldek --up || poldek --upa)>. the alias was created because the poldek indexes are *CONSTANTLY* broken.

=item B<upaa>

upa is alias for C<upa && poldek --upgrade->, which means update all poldek indexes and in case of success update all packages for distribution.

=item B<showline> I<FILE:LINE [FILE:LINE]>

show context of FILE:LINE of 5 lines. colorized if ran on terminal.

=item B<ssh-user>

login to ssh server using using sudo information to figure out real user. it is useful to cvs+ssh using root.
To use ssh+cvs using sush (sudo wrapper):

  export CVS_RSH=ssh-user

=item B<sshcvs>

ssh to cvs server via ssh and chdir to cvs root. first looks into C<$CVSROOT> and it that is unset looks at C<CVS/Root> file.

=back

=head1 SCREEN

screenrc is overriden from the system via C<SYSSCREENRC> variable. but not all
settings are activated because people tend to have custom taste of settings.

so, if you want cool statusbar in screen, you should enable in B<~/.screenrc>:

 caption always

You could also check the other statusbars from B</usr/share/okas/screenrc>.
Copy one to your B<~/.screenrc> and restart screen.

B<NOTE:> You could change these runtime, using B<E<lt>ESCE<gt>+:>, but the screen buffer
is too small to paste there, so you have to restart screen to see effects.

also, you probably didn't know, but you can switch windows in screen by typing window name:
try: B<E<lt>ESCE<gt>+'>. similiar one is B<E<lt>ESCE<gt>+">.

inside screen, you can create ssh connection to another host, type
B<E<lt>ESCE<gt>+s> and enter hostname you want to ssh. this is different from
just typing 'ssh' from command prompt, because it changes window title, and it
will not fork bash to do ssh. so after you end the ssh, or it gets terminate,
you could type 'r' to restore the ssh connection!

B<PS:> B<E<lt>ESCE<gt>> denotes screen escape, which by default is B<E<lt>CTRLE<gt>+a>.

=head1 AUTHOR

Elan RuusamE<auml>e <glen@delfi.ee>

=cut

# vim:ft=pod
