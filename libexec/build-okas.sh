#!/bin/sh
# Builds rpm from the files in Git
# Author: glen@alkohol.ee

set -xeu

die() {
	echo >&2 "$0: $*"
	exit 1
}

# check build deps
rc=0
for pkg in rpm-specdump fakeroot alien dpkg debhelper gawk; do
	o=$(rpm -q $pkg) || { rc=$?; echo >&2 "$o"; }
done
test "$rc" -gt 0 && exit $rc

pkg_name=okas

if [ -n "${CI_COMMIT_TAG:-}" ]; then
	pkg_version=${CI_COMMIT_TAG#v}
	pkg_release=1
else
	git=$(git describe --tags || echo 0.0.1)
	# resolve git warning: tag 'jenkins-okas-184' is really 'v1.6-184' here
	git=$(echo $git | sed -e 's/jenkins-okas-/v1.6-/')

	# strip commit count and commit hash
	pkg_version=${git%-*-*}
	# strip 'v' prefix
	pkg_version=${pkg_version#v}
	# strip rpm release "-1"
	pkg_version=${pkg_version%-*}
	if [ -n "${CI_COMMIT_SHA:-}" ]; then
		sha=$(echo $CI_COMMIT_SHA | cut -c-7)
		pkg_release=1.p$CI_PIPELINE_ID.b$CI_JOB_ID.g$sha
	else
		sha=${git#v$pkg_version-*-}
		pkg_release=1.${git#v$pkg_version-*-}
	fi
fi

# build rpm
_topdir="${PWD:-$(pwd)}"
dist_dir=$_topdir/dist

test -f build/etc/bashrc.local || die "Not properly installed to build dir"

rm -rf $dist_dir
install -d $dist_dir

unset TMPDIR || :

# add changelog similarily to pld builder script
# https://github.com/pld-linux/rpm-build-tools/blob/b34dcd91d2ca8fdbd70a38f39ded002f80d20cca/builder.sh#L460
specfile=$(mktemp)
gitlog=$(mktemp)
cat okas.spec > $specfile
# rpm5.org/rpm.org do not parse any other date format than 'Wed Jan 1 1997'
# otherwise i'd use --date=iso here
# http://rpm5.org/cvs/fileview?f=rpm/build/parseChangelog.c&v=2.44.2.1
# http://rpm.org/gitweb?p=rpm.git;a=blob;f=build/parseChangelog.c;h=56ba69daa41d65ec9fd18c9f371b8ff14118cdca;hb=a113baa510a004476edc44b5ebaaf559238a18b6#l33
# NOTE: changelog date is always in UTC for rpmbuild
# * 1265749244 +0000 Random Hacker <nikt@pld-linux.org> 9370900
git rev-list --date-order -${log_entries:-20} HEAD 2>/dev/null | while read sha1; do
	logfmt='%B%n'
	git notes list $sha1 > /dev/null 2>&1 && logfmt='%N'
	git log -n 1 $sha1 --format=format:"* %ad %an <%ae> %h%n- ${logfmt}%n" --date=raw | sed -re 's/^- +- */- /'| sed '/^$/q'
done > $gitlog

echo '%changelog' >> $specfile
LC_ALL=C gawk '/^\* /{printf("* %s %s\n", strftime("%a %b %d %Y", $2), substr($0, length($1)+length($2)+length($3)+4)); next}{print}' $gitlog >> $specfile

rpmbuild \
	--define "_topdir $_topdir" \
	--define '_builddir %{_topdir}' \
	--define "_rpmdir $dist_dir" \
	--define '_sourcedir %{_topdir}' \
	--define '_specdir %{_topdir}' \
	--define '_buildchangelogtruncate %{nil}' \
	--define '_build_name_fmt %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm' \
	--define '_prefix /usr' \
	--define 'buildroot %{_topdir}/build' \
	--define 'build %%build \
	exit 0%{nil}' \
	--define 'install %%install \
	exit 0%{nil}' \
	--define "version $pkg_version" \
	--define "release $pkg_release" \
	--define 'clean %%clean \
	exit 0%{nil}' \
	--define '__spec_clean_body %{nil}' \
	--define '__spec_install_pre %{nil}' \
	-bb $specfile

rm $specfile

rpm=$(ls $dist_dir/*.rpm 2>/dev/null)
if [ ! -f "$rpm" ]; then
	echo >&2 "RPM not built!"
	exit 1
fi

# gnupg signing
key=$(rpm --eval '%{_gpg_name}')
if [ "$key" ] && [ "$key" != '%{_gpg_name}' ]; then
	echo "Signing package with gpg key $key"
	rpm --addsign "$rpm"
fi

# build debian
if [ ! -x /usr/bin/fakeroot ]; then
	echo >&2 "!! fakeroot not found; debian pkg will be skipped"
	exit 0
fi

V=$(rpm -qp --qf '%{V}' $rpm)
rm -rf okas-$V
alien --scripts -d -g -s -k $rpm
cp -p okas-$V/debian/changelog debian
cp -p okas-$V/debian/copyright debian
fakeroot debian/rules binary

# build apk
# could build from .rpm, but that adds rpmlib() dependencies
# and could not find option to disable these
deb=$(ls $dist_dir/*.deb 2>/dev/null)
url="https://glen.alkohol.ee/okas/"
LC_ALL=en_US.UTF-8 fpm -s deb -t apk --url "$url" -p $dist_dir/$pkg_name-$pkg_version-$pkg_release.apk $deb

exit 0
