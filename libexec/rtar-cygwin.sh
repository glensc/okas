#!/bin/sh
# $Id$
# rtar explorer interface for win32
# Author: glen@delfi.ee
# Date: 9/8/2002

tmp=/tmp/escp$$

target="$1"

perl -e '
	$_ = $ENV{CMDARGS};
	push(@row, defined($1) ? $1:$3) while m/"([^"\\]*(\\.[^"\\]*)*)"|([^ ]+)/g;
	shift(@row);
	map {
		s#\\#/#g;
		s/\047/\047"\047"\047/g;
		($dir, $_) = (m#^(.*/)?(.+?)$#);
		s/^.*$/\047$&\047/;
		printf("%s ", $_);
	} @row;
	printf(";\ndir=\047$dir\047;\n");
' > $tmp

eval set -- "`cat $tmp`"

udir=`cd "$dir"; pwd`
echo "Creating tar:"
tar -cvf $tmp -C "$udir" "$@"
echo ""
echo "Source dir: \`$dir'"
echo "Copying tar to $target:tar-in.tar"
echo ""
ls -log "$tmp"
echo ""
scp "$tmp" "${target}:rtar-in.tar" || read x
rm -f $tmp

# vim:ts=4:sw=4:ai
