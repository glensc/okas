#!/bin/sh
set -xeu

pkg_name=$CI_PROJECT_NAME

rpm=$(basename dist/${pkg_name}-*.noarch.rpm)
deb=$(basename dist/${pkg_name}_*_all.deb)
apk=$(basename dist/${pkg_name}-*.apk)

cp -pi dist/$rpm dist/$deb dist/$apk "$OKAS_DIST_DIR"
cd "$OKAS_DIST_DIR"

# poldek repo
poldek_index() {
	poldek --mkidx -s . --mt=pndir
}

# apt repo (debian/ubuntu)
dpkg_index() {
	dpkg-scanpackages . /dev/null > .Packages.tmp
	mv .Packages.tmp Packages

	# create Packgages.bz2
	bzip2 -9 < Packages > .Packages.tmp.bz2
	mv .Packages.tmp.bz2 Packages.bz2

	gzip -9 < Packages > .Packages.tmp.gz
	mv .Packages.tmp.gz Packages.gz
}

# yum repo
yum_index() {
	createrepo . || :
}

# alpine index
apk_index() {
	local apk="$1"
	apk index -x APKINDEX.tar.gz -o APKINDEX.tar.gz -d 'okas repository' --allow-untrusted $apk
}

# cleanup quick links as they create bogus dupliates to index
rm -f $pkg_name.rpm $pkg_name.deb $pkg_name.apk

poldek_index
dpkg_index
# disabled createrepo; timeouts after 1h
#yum_index
apk_index "$apk"

# for manual quick download
ln -sf $rpm $pkg_name.rpm
ln -sf $deb $pkg_name.deb
ln -sf $apk $pkg_name.apk
