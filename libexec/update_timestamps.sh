#!/bin/sh

set -eu

# update timestamps from last commit
# http://stackoverflow.com/questions/1964470/whats-the-equivalent-of-use-commit-times-for-git/5531813#5531813
update_timestamps() {
	local dir="${1:-.}"
	set +x
	echo "Updating timestamps from last commit, please wait..."
	git --version
	git ls-files | while read file; do
		rev=$(git rev-list -n 1 HEAD -- "$file")
		file_time=$(git show --pretty=format:%ai --abbrev-commit $rev | head -n 1)
		touch -d "$file_time" "$dir/$file"
	done
}

update_timestamps "$@"
