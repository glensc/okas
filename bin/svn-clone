#!/bin/sh
# https://wiki.delfi.net/support:svn_data_importing#svn_import_with_svnsync
# Author: Elan Ruusamäe <glen@delfi.ee>
# Date: $Date$
# Revision: $Revision$

set -e

if [ $# != 2 ]; then
	cat >&2 <<-EOF
	Usage: $0 LOCAL-PATH REMOTE-URL

	Uses svnsync(1) to clone REMOTE-URL to LOCAL-PATH as svn repository

	EOF
	exit 1
fi

svnrepo=$1
svnurl=$2

# make svnrepo full path
svnrepo=$(readlink -f "$svnrepo");

if [ -d "$svnrepo" ]; then
	cat <<-EOF
	$0: ERROR $svnrepo already exists, use to resync, invoke:

	svnsync synchronize file://$svnrepo
	EOF
	exit 1
fi

svnadmin create "$svnrepo"

if [ -e "$svnrepo/hooks/pre-revprop-change" ]; then
	echo "$0: ERROR: hooks/pre-revprop-change already exists"
	exit 1
fi

cat <<-'EOF' > $svnrepo/hooks/pre-revprop-change
#!/bin/sh
REPOS="$1"
REV="$2"
USER="$3"
PROPNAME="$4"
ACTION="$5"

if [ "$ACTION" = "M" -a "$PROPNAME" = "svn:log" ]; then exit 0; fi
if [ "$ACTION" = "A" -a "$PROPNAME" = "svn:sync-lock" ]; then exit 0; fi
if [ "$ACTION" = "A" -a "$PROPNAME" = "svn:sync-from-url" ]; then exit 0; fi
if [ "$ACTION" = "A" -a "$PROPNAME" = "svn:sync-from-uuid" ]; then exit 0; fi
if [ "$ACTION" = "A" -a "$PROPNAME" = "svn:sync-last-merged-rev" ]; then exit 0; fi
if [ "$ACTION" = "M" -a "$PROPNAME" = "svn:date" ]; then exit 0; fi
if [ "$ACTION" = "D" -a "$PROPNAME" = "svn:sync-lock" ]; then exit 0; fi
if [ "$ACTION" = "A" -a "$PROPNAME" = "svn:sync-currently-copying" ]; then exit 0; fi
if [ "$ACTION" = "M" -a "$PROPNAME" = "svn:author" ]; then exit 0; fi
if [ "$ACTION" = "M" -a "$PROPNAME" = "svn:sync-last-merged-rev" ]; then exit 0; fi
if [ "$ACTION" = "D" -a "$PROPNAME" = "svn:sync-currently-copying" ]; then exit 0; fi

echo "Changing revision properties other than svn:log is prohibited ($ACTION $PROPNAME)" >&2
exit 1
EOF
chmod a+rx $svnrepo/hooks/pre-revprop-change

svnsync initialize "file://$svnrepo" "$svnurl"
svnsync synchronize "file:///$svnrepo"
