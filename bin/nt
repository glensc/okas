#!/bin/sh
# NT: print netstat statistics
# Author: glen@delfi.ee

LC_ALL=C ss -ant | PERL_BADLANG=0 perl -ane '
BEGIN {
	my %m = (
		ESTABLISHED => "ESTAB",
		SYN_SENT => "SYN-SENT",
		SYN_RECV => "SYN-RECV",
		FIN_WAIT1 => "FIN-WAIT-1",
		FIN_WAIT2 => "FIN-WAIT-2",
		TIME_WAIT => "TIME-WAIT",
		CLOSE => "UNCONN",
		CLOSE_WAIT => "CLOSE-WAIT",
		LAST_ACK => "LAST-ACK",
		LISTEN =>  "LISTEN",
		CLOSING => "CLOSING",
	);

	# reset values to 0
	%a = map { $_ => 0 } values %m unless -t STDOUT;

	# create reverse map for pretty print
	%r = map { $m{$_} => $_ } keys %m;
}

++$a{$F[0]} if $F[0] =~ /^[A-Z0-9-]+$/;

END {
	my $t = $p = 0;

	foreach my $k (sort { $a{$b} <=> $a{$a} || $b cmp $a } keys %a) {
		my $v = $a{$k};
		$k = $r{$k};
		$t += $v;
		$p = $p > length($v) ? $p : length($v);
		printf("%15s: %d\n", $k, $v);
	}
	$p = $p > length($t) ? $p : length($t);
	print "-" x (15 + 2 + $p), "\n";
	printf("%15s: %d\n", "Total", $t);
}'
