class Okas < Formula
  desc "okas"
  homepage "https://glen.alkohol.ee/okas/"
  url "https://gitlab.com/glensc/okas/-/archive/v1.8.1/okas-v1.8.1.tar.bz2"
  sha256 "78add53bd3173b5f2a6f6233388fb46b2763b214337cee19fb00b6a2c18b17b5"
  head "https://gitlab.com/glensc/okas.git"

  def install
    system "make", "install", "prefix=#{prefix}", "sysconfdir=#{prefix}/etc"
  end
end

# vim:ts=2:sw=2:et
