#! /bin/bash
#
# cdhist - cd replacement with a directory stack like pushd/popd
#
# usage: cd [-l] [-n] [-] [dir]
#
# options:
#	-l	print the cd directory stack, one entry per line
#	-	equivalent to $OLDPWD
#	-n	cd to nth directory in cd directory stack
#	-s	cd to first directory in stack matching (substring) `s'
#
# arguments:
#	dir	cd to dir and push dir onto the cd directory stack
#
# If the new directory is a directory in the stack and the options selected
# it (-n, -s), the new working directory is printed
#
# If the variable CDHISTFILE is set, the cd directory stack is loaded from
# and written to $CDHISTFILE every time `cd' is executed.
#
# Note: I got this off the net somewhere; I don't know the original author
#
# Chet Ramey
# chet@po.cwru.edu
#
# 2000-12-26: adopted by glen@delfi.ee
# 2019-01-06: glen@pld-linux.org: changed to use chpwd_functions

okas_cd_read_history() {
	# if directory history exists
	if [ -z "$CDHISTFILE" ] || [ ! -r "$CDHISTFILE" ]; then
		return
	fi

	local t
	typeset -i i=-1
	# read directory history file
	while read -r t; do
		CDHIST[i=i+1]=$t
	done < $CDHISTFILE
}

okas_cd_write_history() {
	if [ -z "$CDHISTFILE" ]; then
		return
	fi

	# number of elements in history
	typeset -i cdlen=${#CDHIST[*]} i=0

	# update directory history
	while ((i<cdlen)); do
		echo ${CDHIST[i]}
		((i=i+1))
	done > $CDHISTFILE
}

okas_cd_match() {
	local t="$1"
	declare -i i=1

	while ((i<cdlen)); do
		case ${CDHIST[i]} in
		*$t*)
			args=("${CDHIST[i]}")
			break
			;;
		esac
		((i=i+1))
	done
	if ((i>=cdlen)); then
		args=("$@")
	fi
}

# print directory list
okas_cd_print_dirs() {
	typeset -i i
	((i=cdlen))
	while (((i=i-1)>=0)); do
		printf "%d %s\n" $i "${CDHIST[i]}"
	done
}

okas_cd_handler() {
	typeset -i cdlen i result=0
	typeset t
	typeset -a CDHIST args

	if [ $# -eq 0 ]; then
		set -- "$HOME"
	fi

	args=("$@")

	okas_cd_read_history
	okas_cd_insert

	# number of elements in history
	cdlen=${#CDHIST[*]}

	case "$1" in
	-)
		# cd to new dir
		if [ "$OLDPWD" = "" ] && ((cdlen>1)); then
			args=("${CDHIST[1]}")
		fi
		;;
	-l)
		okas_cd_print_dirs
		return 0
		;;
	-[0-9]|-[0-9][0-9])
		# cd to dir in list
		if (((i=${1#-})<cdlen)); then
			args=("${CDHIST[i]}")
		fi
		;;
	-*)
		# cd to matched dir in list
		okas_cd_match "${1#-}"
		;;
	esac

	__zsh_like_cd cd "${args[@]}" && result=$? || result=$?

	okas_cd_insert
	okas_cd_write_history

	return $result
}

# insert $PWD into cd history
# meant to be called only by cd
okas_cd_insert() {
	if [ "${CDHIST[0]}" = "$PWD" ] || [ "$PWD" = "" ]; then
		return
	fi

	# insert $PWD into cd history
	typeset -i i=0

	# see if dir is already in list
	while (( i < ${#CDHIST[*]} )); do
		if [ "${CDHIST[$i]}" = "$PWD" ]; then
			break
		fi
		((i=i+1))
	done

	# limit max size of list
	if (( i>22 )); then
		i=22
	fi

	# bump old dirs in list
	while (((i=i-1)>=0)); do
		CDHIST[i+1]=${CDHIST[i]}
	done

	# insert new directory in list
	CDHIST[0]=$PWD
}

cd() {
	okas_cd_handler "$@"
}
