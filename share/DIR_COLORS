# vim:filetype=dircolors
#
# Configuration file for the color ls utility
# This file goes in the /etc directory, and must be world readable.
# You can copy this file to .dir_colors in your $HOME directory to override
# the system defaults.

# COLOR needs one of these arguments: 'tty' colorizes output to ttys, but not
# pipes. 'all' adds color characters to all output. 'none' shuts colorization
# off.
COLOR tty

# Extra command line options for ls go here.
# Basically these ones are:
#  -F = show '/' for dirs, '*' for executables, etc.
#  -T 0 = don't trust tab spacing when formatting ls output.
OPTIONS -F -T 0

# Below, there should be one TERM entry for each termtype that is colorizable
TERM Eterm
TERM ansi
TERM color-xterm
TERM color_xterm
TERM con132x25
TERM con132x30
TERM con132x43
TERM con132x60
TERM con80x25
TERM con80x28
TERM con80x30
TERM con80x43
TERM con80x50
TERM con80x60
TERM cons25
TERM console
TERM dtterm
TERM gnome
TERM kon
TERM kterm
TERM linux
TERM linux-c
TERM mach-color
TERM rxvt
TERM rxvt-unicode
TERM screen
TERM screen-256color
TERM screen-bce
TERM screen-w
TERM screen.linux
TERM vt100
TERM vt102
TERM xterm
TERM xterm-256color
TERM xterm-color
TERM xterm-debian

# EIGHTBIT, followed by '1' for on, '0' for off. (8-bit output)
EIGHTBIT 1

# Below are the color init strings for the basic file types. A color init
# string consists of one or more of the following numeric codes:
# Attribute codes:
# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes:
# 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white
# Background color codes:
# 40=black 41=red 42=green 43=yellow 44=blue 45=magenta 46=cyan 47=white
NORMAL 00       # global default, although everything should be something.
FILE   00       # normal file
DIR    01;34    # directory
LINK   00;36    # symbolic link.  (If you set this to 'target' instead of a
                # numerical value, the color is as for the file pointed to.)
FIFO   40;33    # pipe
SOCK   01;35    # socket
#DOOR   01;35    # door
BLK    40;33;01 # block device driver
CHR    40;33;01 # character device driver
ORPHAN  01;05;37;41  # orphaned syminks
MISSING 01;05;37;41 # ... and the files they point to


# This is for files with execute permission:
EXEC 00;32

# List any file extensions like '.gz' or '.tar' that you would like ls
# to colorize below. Put the extension, a space, and the color init string.
# (and any comments you want to add after a '#')

# 01;30 is still free

# executables (bright green)
.sh   01;32
.csh  01;32
.cmd  01;32
.exe  01;32
.com  01;32
.bat  01;32
.btm  01;32
.dll  01;32

# archives or compressed (red)
.7z   00;31
.Z    00;31
.ace  00;31
.apk  00;31
.arj  00;31
.bz   00;31
.bz2  00;31
.cpio 00;31
.deb  00;31
.gem  00;31
.gz   00;31
.img  00;31
.jar  00;31
.lha  00;31
.lrz  00;31
.lzh  00;31
.lzma 00;31
.phar 00;31
.rar  00;31
.rpm  00;31
.rz   00;31
.sqf  00;31
.tar  00;31
.taz  00;31
.tb2  00;31
.tbz  00;31
.tbz2 00;31
.tgz  00;31
.tpz  00;31
.trpm 00;31
.tz   00;31
.tz2  00;31
.war  00;31
.whl  00;31
.xz   00;31
.z    00;31
.zip  00;31
.zoo  00;31
.zst  00;31

# image formats (bright magenta)
.JPG  01;35
.bmp  01;35
.cdr  01;35
.gif  01;35
.jpeg 01;35
.jpg  01;35
.mng  01;35
.pbm  01;35
.pcx  01;35
.pgm  01;35
.png  01;35
.ppm  01;35
.svg  01;35
.tga  01;35
.tif  01;35
.tiff 01;35
.xbm  01;35
.xcf  01;35
.xpm  01;35
.xwd  01;35

# multimedia files (magenta)
.3gp  00;35
.aac  00;35
.aiff 00;35
.asf  00;35 # Advanced Systems Format (contains Windows Media Video)
.asx  00;35
.au   00;35
.avi  00;35
.AVI  00;35
.dl   00;35
.flac 00;35
.flc  00;35 # AutoDesk Animator
.fli  00;35 # AutoDesk Animator
.flv  00;35
.gl   00;35
.m1v  00;35
.m2v  00;35 # MPEG-2 Video only
.m4a  00;35 # MPEG-4 Audio only
.m4r  00;35 # MPEG-4 Ringtone
.m4v  00;35 # MPEG-4 Video only
.mid  00;35
.midi 00;35
.mkv  00;35 # Matroska (http://matroska.org/)
.mod  00;35
.mov  00;35 # Quicktime (http://developer.apple.com/qa/qtw/qtw99.html)
.mp2  00;35
.mp3  00;35
.mp4  00;35 # "Offical" container for MPEG-4
.mp4v 00;35 # MPEG-4 Video only
.mpe  00;35
.mpeg 00;35
.mpg  00;35
.nuv  00;35
.ogg  00;35
.ogm  00;35 # Ogg Media File
.ogv  00;35
.opus 00;35
.qt   00;35 # Quicktime (http://developer.apple.com/qa/qtw/qtw99.html)
.ram  00;35
.rm   00;35 # Real Media
.rmvb 00;35 # Real Media Variable Bitrate
.s3m  00;35
.swf  00;35
.vob  00;35
.voc  00;35
.wav  00;35
.webm 00;35
.wma  00;35
.wmv  00;35 # Windows Media Video
.xm   00;35
.yuv  00;35

# documents (bright white)
.abw     01
.bib     01
.css     01
.doc     01
.dot     01
.dvi     01
.eps     01
.htm     01
.html    01
.info    01
.kpr     01
.log     01
.me      01
.ms      01
.odb     01
.odc     01
.odf     01
.odg     01
.odi     01
.odp     01
.ods     01
.odt     01
.otc     01
.otf     01
.otg     01
.oth     01
.oti     01
.otm     01
.otp     01
.ots     01
.ott     01
.pdf     01
.pps     01
.ppt     01
.ps      01
.rtf     01
.sql     01
.sxc     01
.sxw     01
.tex     01
.texi    01
.texinfo 01
.tm      01
.tmpl    01
.txt     01
.wri     01
.xls     01
.xlw     01
.xml     01
.xsl     01

# devel (gray)
.ac    01;30
.am    01;30
.c     01;30
.cc    01;30
.cpp   01;30
.diff  01;30
.f     01;30
.h     01;30
.in    01;30
.o     01;30
.patch 01;30
.po    01;30
.pot   01;30
.s     01;30
.spec  01;30

# PHP sources
.inc   01
.php   01;30

# PGP armored data
.asc   01;31
