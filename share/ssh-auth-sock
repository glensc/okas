#!/bin/sh
# function that tries very hard to make your $SSH_AUTH_SOCK valid
# call this func from your ~/.bash_profile
# you need keychain >= 2.6.0 installed for this to work.
#
ssh-auth-sock() {
	local have_sock=0 user_sock="$1"
	if [ -S "$SSH_AUTH_SOCK" -a -n "$SSH_AGENT_PID" ]; then
		if [ -d /proc/$SSH_AGENT_PID ]; then
			have_sock=1
		else
			unset SSH_AGENT_PID
		fi
	fi

	# check socket with ssh-add -l:
	# 0 means agent up and has keys
	# 1 means agent up and no keys
	if [ $have_sock = 0 ]; then
		local st=0
		ssh-add -l >/dev/null 2>&1 || st=$?
		if [ "$st" = 0 -o $st = 1 ]; then
			have_sock=1
		fi
	fi

	if [ $have_sock = 1 ] && which keychain >/dev/null 2>&1; then
		# we have working socket, great, save it for latter with keychain
		keychain -q --agents ssh --inherit any
		return
	fi

	# no socket, but user supplied socket, update
	if [ -z "$SSH_AUTH_SOCK" ] && [ -S "$user_sock" ]; then
		export SSH_AUTH_SOCK=$user_sock
		if [ -L ~/.byobu/.ssh-agent ]; then
			ln -snf "$SSH_AUTH_SOCK" ~/.byobu/.ssh-agent
		fi
	fi

	# so, we had invalid socket?
	if [ "$SSH_AUTH_SOCK" ] && [ ! -S "$SSH_AUTH_SOCK" ]; then
		local sock
		if [ "$user_sock" ]; then
			sock=$user_sock
		else
			sock=$(unset SSH_AUTH_SOCK; . $HOME/.keychain/$HOSTNAME-sh 2>/dev/null; echo $SSH_AUTH_SOCK)
		fi

		if [ -z "$sock" ]; then
			# empty $sock. try user supplied one
			[ -S "$user_sock" ] && sock=$user_sock
			# TODO should start keychain here?
		fi

		local sockdir=$(dirname $SSH_AUTH_SOCK)
		# remove invalid symlink and empty dir (ignore if nothing exists)
		rm -f $sockdir 2>/dev/null || rmdir $sockdir 2>/dev/null

		# dir still exists?
		if [ -d $sockdir -a ! -L $sockdir ]; then
			# dir might still exist and empty after we tried to rmdir empty dir if it's bind mounted
			# as it's directory, must symlink with full path
			ln -sf $sock $sockdir
			ln -sf $(basename $sock) $SSH_AUTH_SOCK
		else
			# dir not there
			if [ -S "$sock" ]; then
				# great! can use relative paths
				if ln -snf $(dirname $sock) $sockdir; then
					ln -snf $(basename $sock) $SSH_AUTH_SOCK
				else
					# failed to symlink, so perhaps just can't remove symlink as it's root owned?
					ln -sf $sock $SSH_AUTH_SOCK
				fi

			else
				echo >&2 "ssh-auth-sock: No usable new or old socket"
				return 2
			fi
		fi

		# but wait. lets set the sock to proper one!
		if [ -S $sock ]; then
			export SSH_AUTH_SOCK=$sock
		fi
		return
	fi

	# no $SSH_AUTH_SOCK. inherit one!
	if [ -f ~/.keychain/$HOSTNAME-sh ]; then
		. ~/.keychain/$HOSTNAME-sh
		# recurse to validate!
		ssh-auth-sock
		return
	fi

	# try hostname -- perhaps hostname has changed
	local hostname=$(hostname)
	if [ -f ~/.keychain/$hostname-sh ]; then
		. ~/.keychain/$hostname-sh
		# save it for later case
		keychain -q --agents ssh --inherit any
		return
	fi

	# try byobu socket
	if [ -S ~/.byobu/.ssh-agent ]; then
		export SSH_AUTH_SOCK=~/.byobu/.ssh-agent
		echo >&2 "ssh-auth-sock: Using $SSH_AUTH_SOCK"
		if [ -n "${TMUX:-}" ]; then
			echo >&2 "ssh-auth-sock: Updating tmux SSH_AUTH_SOCK env"
			tmux setenv SSH_AUTH_SOCK "$SSH_AUTH_SOCK"
		fi
		return
	fi

	echo >&2 "ssh-auth-sock: keychain file not found"
}
