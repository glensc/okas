#! awk

BEGIN {
	user = ENVIRON["SUDO_USER"];
	if (!user) {
		user = ENVIRON["USER"];
	}
}

{
	if (NR == 1) {
		#Kasutaja Nimi                 TTY      Eemal  Millal       Kust
		#Login     Name               Tty      Idle  Login Time   Office     Office Phone
		ptty = index($0, $3);
	}

	pname = index($0, $2);

	login = substr($0, 0, pname - 1);
	name = substr($0, pname, ptty - pname - 1);
	wall = substr($0, ptty - 1, 1);
	rest = substr($0, ptty);
	if ($1 == "root") {
		root = "\033[41m"
		rest = "\033[37m" rest
	} else if ($1 == user) {
		root = "\033[44m"
		rest = "\033[37m" rest
	} else {
		root = "";
		rest = "\033[39m" rest
	}

	if (NR == 1) {
		printf("%-11s%s%s%s\n", login, name, wall, rest);
	} else {
		printf("%s\033[32m%-11s\033[33m%s\033[31m%s%s\033[0m\n", root, login, name, wall, rest);
	}
}
