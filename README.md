# okas

## Installation

### Alpine Linux

```sh
apk add okas --allow-untrusted --repository https://glen.alkohol.ee/okas/dist --update
```

### Debian/Ubuntu flavors

```sh
echo "deb [trusted=yes] https://glen.alkohol.ee/okas/dist ./" | sudo tee -a /etc/apt/sources.list.d/okas.list
sudo apt-get update
sudo apt-get install okas
```

### Homebrew (MacOS) / Linuxbrew

```sh
brew tap glensc/okas https://gitlab.com/glensc/okas.git
brew install --HEAD okas
```

### PLD Linux

```sh
poldek -s https://glen.alkohol.ee/okas/dist/ -u okas
```

### RPM Manual install

```sh
wget https://glen.alkohol.ee/okas/dist/okas.rpm
rpm -Uhv okas.rpm
```

## Docker

The docker image is also available. It's not usable standalone (`docker run`),
but for copying files using [multistage build] available from 17.05:

```dockerfile
FROM alpine:latest
COPY --from=registry.gitlab.com/glensc/okas:latest / /
CMD ["sh"]
```

[multistage build]: https://docs.docker.com/develop/develop-images/multistage-build/
