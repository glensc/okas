# FHS paths
prefix		:= /usr
ifeq ($(shell uname), Darwin)
prefix := $(prefix)/local
endif
sysconfdir	:= /etc
libdir		:= $(prefix)/lib
sbindir		:= $(prefix)/sbin
bindir		:= $(prefix)/bin
mandir		:= $(prefix)/share/man
datadir		:= $(prefix)/share
vimdatadir	:= $(prefix)/share/vim/vimfiles

all: manuals

manuals:
	$(MAKE) -C man

install: install-files install-man install-contrib

install-man: manuals install-dirs
	cp -a man/man5/* $(DESTDIR)$(mandir)/man5
	cp -a man/man1/* $(DESTDIR)$(mandir)/man1

install-dirs:
	install -d $(DESTDIR)$(bindir)
	install -d $(DESTDIR)$(sbindir)
	install -d $(DESTDIR)$(sysconfdir)
	install -d $(DESTDIR)$(datadir)/okas/vim
	install -d $(DESTDIR)$(datadir)/okas/contrib/bash_zsh_support/chpwd
	install -d $(DESTDIR)$(datadir)/okas/contrib/bash-preexec
	install -d $(DESTDIR)$(libdir)/okas
	install -d $(DESTDIR)$(mandir)/man1
	install -d $(DESTDIR)$(mandir)/man5

install-files: install-dirs
	cp -a bin/* $(DESTDIR)$(bindir)
	cp -a sbin/* $(DESTDIR)$(sbindir)
	cp -a share/* $(DESTDIR)$(datadir)/okas
	cp -a lib/* $(DESTDIR)$(libdir)/okas
	cp -a etc/bashrc.local $(DESTDIR)$(sysconfdir)

install-contrib: install-dirs
	cp -a contrib/git-fixup/git-fixup $(DESTDIR)$(bindir)
	cp -a contrib/git-mr/git-mr $(DESTDIR)$(bindir)
	cp -a contrib/git-mr/git-pr $(DESTDIR)$(bindir)
	cp -a contrib/git-fire/git-fire $(DESTDIR)$(bindir)
	cp -a contrib/devscripts/scripts/annotate-output.sh $(DESTDIR)$(bindir)/fannotate
	cp -a contrib/devscripts/scripts/annotate-output.1 $(DESTDIR)$(mandir)/man1/fannotate.1
	cp -p contrib/bash_zsh_support/chpwd/*.sh $(DESTDIR)$(datadir)/okas/contrib/bash_zsh_support/chpwd
	cp -p contrib/bash-preexec/bash-preexec.sh $(DESTDIR)$(datadir)/okas/contrib/bash-preexec
